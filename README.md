# The shape of interactions

A very minimal, to be extended, simulation of groups behavior which can be used during workshops and teaching.
Goal is to show and think about the importance of context in interactions and to wonder if there's really any relevant "natural" behavior considering how one can change when the context changes, even though their basic functions are hardcoded.
Participation in the simulation is possible through mouse clicks but optionnal.

One of the craziest extension plans could be to port the base in 3D and develop a full mini-amateur-game (see previous commits) that could help even more in workshops.

## User side

### How to play

You can download the whole repo and build the game yourself if you know how to or just launch the godot project and play the main scene.

## Dev side

### Contributing / roadmap

For now, roadmaps are limitated to GitLab issues. Could be improved if exterior contributions were to happen and such. Contribution is more or less freestyle now, could be improved if some were to happen. Contributions would be more than necessary to make it a game (even minimal, amateur and so on).
