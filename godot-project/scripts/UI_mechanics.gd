extends Node2D
# For now, mainly used to understand better some mechanics (about Signals mainly). 
# Timers and their winkers are over-complicated for nothing, because I learnt from them. At some point, they should be simplified.

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#var script_generic_human_behavior = load("res://generic_human_behavior.gd")
var human_freakyness
var blinker_is_visible = 0 # time in seconds since the blinker is visible
var time_since_launch
var is_timer_on = false

# Called when the node enters the scene tree for the first time.
func _ready():
	time_since_launch = 0
	get_node("UI_utilities/blinker_timer").connect("timeout", self, "_when_blinker_timer_times_out")
	human_freakyness = get_parent().get_node("world_simulation/humanity").adult_normal_freakyness
	get_node("UI_utilities/blinker_timer").wait_time = human_freakyness

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	blinker_is_visible += delta
	time_since_launch += delta
	if blinker_is_visible >= 0.25:
		get_node("Panel/blinker").visible = false
	if time_since_launch >= get_parent().get_node("world_simulation/humanity").spawn_waiting && is_timer_on == false:
		_when_blinker_timer_times_out()
		get_node("UI_utilities/blinker_timer").start()
		is_timer_on = true

func _when_blinker_timer_times_out():
	get_node("Panel/blinker").visible = true
	blinker_is_visible = 0
	
