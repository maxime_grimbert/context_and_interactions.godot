extends Node2D
# Extension Script to create a simulation (create a world) with starting elements

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var initial_adult_humans_per_team = 5 # can be change at will ; export makes it available in the editor
export var initial_team_quantity = 2 # can be change at will ; note that the color given to teams is defined by default for now (can be upgraded later) ; note that for now team is limitated to 1, 2 (for 3 need spwan zones and for 3 and 4 need sprite templates)
export var team_color_1 = "blue"# Allows to change the color of the team ; needs to be one of the 4 colors stated here : blue, yellow, green, red
export var team_color_2 = "yellow"
export var team_color_3 = "green"
export var team_color_4 = "red"
var generic_human = load("res://scenes/generic_human.tscn")
var spawn_zone_1 = [] # A 4 number array that reads like minimum x position minimum y position maximum x position maximum y position like [20, 20, 1900, 1260]
var spawn_zone_2 = []
var spawn_zone_3 = []
var spawn_zone_4 = []

# Called when the node enters the scene tree for the first time.
# Probably the only function to be used in this script as big_bang is supposed to happen only on launch of program
func _ready():
	determine_spawn_zones()
	create_humans()

func determine_spawn_zones():
	if initial_team_quantity == 1:
		spawn_zone_1 = [20, 20, 1260, 700]
	if initial_team_quantity == 2:
		spawn_zone_1 = [20, 20, 620, 700]
		spawn_zone_2 = [660, 20, 1260, 700]
	# if initial_team_number == 1:
		# pseudocode : divide field in 3 parts
	if initial_team_quantity == 4:
		spawn_zone_1 = [20, 20, 620, 340]
		spawn_zone_2 = [660, 20, 1260, 340]
		spawn_zone_3 = [20, 380, 620, 700]
		spawn_zone_4 = [660, 740, 1260, 700]
#	print(spawn_zone_1) # Needed only for test purposes
#	print(spawn_zone_2)
#	print(spawn_zone_3)
#	print(spawn_zone_4)	

# The two funcs below create initial human inviduals
func create_humans(): 
	if initial_team_quantity >= 1 :
		for i in initial_adult_humans_per_team:
			create_human(team_color_1, initial_team_quantity)
	if initial_team_quantity >= 2 :
		for i in initial_adult_humans_per_team:
			create_human(team_color_2, initial_team_quantity)
	if initial_team_quantity >= 3 :
		for i in initial_adult_humans_per_team:
			create_human(team_color_3, initial_team_quantity)
	if initial_team_quantity >= 4 :
		for i in initial_adult_humans_per_team:
			create_human(team_color_4, initial_team_quantity)

func create_human(team_color, team_quantity):
	var individual_human = generic_human.instance() # create in the memory an individual human based on the generic human
	# Below we deal with the position of the created human individual. Its position depends on the team color and the number of team.
	# Starting by generating 2 randoms numbers :
	var rd1 = RandomNumberGenerator.new() # rd1 stands for random 1 ; this line and the next two lines are used to generate a pseudo-random number between 25 and 615
	rd1.randomize()
	var rd2 = RandomNumberGenerator.new()
	rd2.randomize()
	# Make those two numbers part of a range that goes from minimum x to maximum x and from minimum y to maximum y depending of the spawning zones :
	if team_quantity == 1: 
		var rrd1 = rd1.randf_range(spawn_zone_1[0], spawn_zone_1[2]) # rrd1 stands for randomized random  1 ; since rrd1 will be an x position of the new individual human, it has to spawn not in the wall and not on the ennemy lines ; and the range for x and y depends on the spawn zones which depends on  
		individual_human.position.x = rrd1 
		var rrd2 = rd2.randf_range(spawn_zone_1[1], spawn_zone_1[3])
		individual_human.position.y = rrd2 # we give the new indivudal human positions before adding it to the scene so it doesn't pop and move but spawn directly where we want
	if team_quantity == 2:
		if team_color == "blue":
			var rrd1 = rd1.randf_range(spawn_zone_1[0], spawn_zone_1[2]) 
			individual_human.position.x = rrd1 
			var rrd2 = rd2.randf_range(spawn_zone_1[1], spawn_zone_1[3])
			individual_human.position.y = rrd2
		if team_color == "yellow":
			var rrd1 = rd1.randf_range(spawn_zone_2[0], spawn_zone_2[2]) 
			individual_human.position.x = rrd1 
			var rrd2 = rd2.randf_range(spawn_zone_2[1], spawn_zone_2[3])
			individual_human.position.y = rrd2
#	if team_quantity == 3: # Depends on not-done-yet spawns zones for 3 teams.
#		if team_color == "blue":
#		if team_color == "yellow":
#		if team_color == "green":
#	if team_quantity == 4: # TODO
#		if team_color == "blue":
#		if team_color == "yellow":
#		if team_color == "green":
#		if team_color == "red":
	
	# give it color by changing the value of the variable in their script
	individual_human.team_color = team_color
	# give it color by changing the texture associated with the sprit of the instanced scene
	if team_color == "blue":
		var individual_texture = load("res://assets/square_32px_#7091F5_blue.png")
		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
	if team_color == "yellow":
		var individual_texture = load("res://assets/square_32px_#FABC5F_yellow.png")
		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
#	if team_color == "green": # The right assets are needed for this
#		var individual_texture = load("res://assets/###")
#		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
#	if team_color == "red":
#		var individual_texture = load("res://assets/###")
#		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)

	individual_human.age = "adult"
	
	get_parent().get_node("humanity").get_node("human_individuals").add_child(individual_human) # we add it to our main scene at the right position
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
