extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Below are variables used for counters
var number_of_humans = 0
var number_of_team_1_humans = 0
var number_of_team_2_humans = 0
var number_of_team_3_humans = 0
var number_of_team_4_humans = 0
# Below are variables used for the spirit's spells and powers
var wall_cooldown = 4
var wall_duration = 4
var is_wall_on_field = false 
var time_since_wall = 0

# Called when the node enters the scene tree for the first time.
func _ready():
# Below we set up the conters
	number_of_humans = get_parent().get_node("world_simulation/humanity/human_individuals").get_child_count()
	number_of_team_1_humans = get_parent().get_node("world_simulation/big_bang").initial_adult_humans_per_team
	number_of_team_2_humans = get_parent().get_node("world_simulation/big_bang").initial_adult_humans_per_team
	number_of_team_3_humans = get_parent().get_node("world_simulation/big_bang").initial_adult_humans_per_team
	number_of_team_4_humans = get_parent().get_node("world_simulation/big_bang").initial_adult_humans_per_team
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Below is the mechanism for updating conters
	number_of_humans = get_parent().get_node("world_simulation/humanity/human_individuals").get_child_count()
	number_of_team_1_humans = 0
	number_of_team_2_humans = 0
	number_of_team_3_humans = 0
	number_of_team_4_humans = 0
		
	for x in number_of_humans:
		if get_parent().get_node("world_simulation/humanity/human_individuals").get_child(x).team_number == 1:
			number_of_team_1_humans += 1
		if get_parent().get_node("world_simulation/humanity/human_individuals").get_child(x).team_number == 2:
			number_of_team_2_humans += 1
		if get_parent().get_node("world_simulation/humanity/human_individuals").get_child(x).team_number == 3:
			number_of_team_3_humans += 1
		if get_parent().get_node("world_simulation/humanity/human_individuals").get_child(x).team_number == 4:
			number_of_team_4_humans += 1
		pass
	# Below we use an «if number of team» structure to allow for updating the panel itself as well as what's written in it and the number of lines used by the conters' text
	if get_parent().get_node("world_simulation/big_bang").initial_team_quantity == 1:
		get_parent().get_node("UI/Panel/counters").text = "Number of \n- humans : " + str(number_of_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_1 + " humans : " + str(number_of_team_1_humans)
	
	if get_parent().get_node("world_simulation/big_bang").initial_team_quantity == 2:
		get_parent().get_node("UI/Panel/counters").text = "Number of \n- humans : " + str(number_of_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_1 + " humans : " + str(number_of_team_1_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_2 + " humans : " + str(number_of_team_2_humans)
		
	if get_parent().get_node("world_simulation/big_bang").initial_team_quantity == 3:
		get_parent().get_node("UI/Panel/counters").text = "Number of \n- humans : " + str(number_of_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_1 + " humans : " + str(number_of_team_1_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_2 + " humans : " + str(number_of_team_2_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_3 + " humans : " + str(number_of_team_3_humans)
		
	if get_parent().get_node("world_simulation/big_bang").initial_team_quantity == 4:
		get_parent().get_node("UI/Panel/counters").text = "Number of \n- humans : " + str(number_of_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_1 + " humans : " + str(number_of_team_1_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_2 + " humans : " + str(number_of_team_2_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_3 + " humans : " + str(number_of_team_3_humans) + "\n- " + get_parent().get_node("world_simulation/big_bang").team_color_4 + " humans : " + str(number_of_team_4_humans)

	#Below is victory / defeat conditions
	if number_of_team_1_humans <= 0 or (number_of_team_2_humans + number_of_team_3_humans + number_of_team_4_humans)/number_of_team_1_humans >= 10:
		print("victory")
		var main_menu = load("res://scenes/main_menu.tscn")
		var new_menu = main_menu.instance()
		get_parent().get_parent().add_child(new_menu)
		new_menu.get_node("Panel/Victory").visible = true
		get_parent().call_deferred("free")
	if number_of_team_2_humans <= 0 && number_of_team_3_humans <= 0 && number_of_team_3_humans <= 0 or number_of_team_1_humans/(number_of_team_2_humans + number_of_team_3_humans + number_of_team_4_humans) >= 10:
		print("defeat")
		var main_menu = load("res://scenes/main_menu.tscn")
		var new_menu = main_menu.instance()
		get_parent().get_parent().add_child(new_menu)
		new_menu.get_node("Panel/Defeat").visible = true
		get_parent().call_deferred("free")

	# Below starts the mechanism to drop walls
	if is_wall_on_field == true:
		time_since_wall += delta
	if time_since_wall >= wall_duration:
		get_child(0).call_deferred("free")
		is_wall_on_field = false
		time_since_wall = 0
	if Input.is_action_pressed("left_click") && is_wall_on_field == false:
		#createwell
		var wall = load("res://scenes/wall.tscn")
		var new_wall = wall.instance()
		new_wall.position = get_global_mouse_position()
		add_child(new_wall)
		is_wall_on_field = true
		pass
