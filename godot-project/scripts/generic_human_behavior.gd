extends KinematicBody2D
# Change-log : We eliminated 8way movement for brand new rotation+forward movement (will be more suitable for future features that require a front and a rear)

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Misc variables below
var team_color = "pink"
var team_number = 0
# Movement variables below
var course = Vector2() # i think its the same as mainstream velocity
var last_course_change = 0 # in seconds, time since last change of course
var adult_normal_speed = 100 # multiplicative factor for the body's movement velocity /!\ the observed relative speed of move_and_slide seems more or less 50 times faster than move_and_collide
var speed = adult_normal_speed # allows to change for say child and get it back after growing up
var adult_normal_freakyness # in seconds, time between two of the body's change of course
var freakyness = adult_normal_freakyness # allows to change for say child and get it back after growing up
var spawn_waiting # in seconds, time after spawn before "activation" (movement only for now, but should be improve as well as far as interactions are concerned)
var time_since_spawn # in seconds

var age = "adult" # for now should be "adult" or "child" (adult by default to avoid mistakes)

var gestation = false # true or false, false by default
var gestation_req_time = 5 # in seconds, the time required for a full gestation
var gestation_time = 0 # in seconds, the current gestation time of this human

var generic_human = load("res://scenes/generic_human.tscn") # used to create new humans by instancing the scene (by making babies)

# Called when the node enters the scene tree for the first time.
func _ready():
	adult_normal_freakyness = get_parent().get_parent().adult_normal_freakyness
	freakyness = adult_normal_freakyness
	spawn_waiting = get_parent().get_parent().spawn_waiting
	# The three above lines have to be here (in _ready() ) instead of when defininf variables because otherwise the script on humanity is not accessible yet.
	time_since_spawn = 0
	set_random_course()
	$body_area.connect("area_entered", self, "an_area_enters_my_body") # This line makes so this script (self) receives the signal from its child body_area which is called area_entered and when this signal arrives, it activates the function an_area_enters_my_body which is stated below.
	# Tokens below that inform on status:
	get_node("tokens/pregnancy").visible = false # incremented below the if condition above for same reason
	
	# Below are lines to make match team color and number (used if other scripts related to game_mechanics)
	if team_color == get_parent().get_parent().get_parent().get_node("big_bang").team_color_1 :
		team_number = 1
	if team_color == get_parent().get_parent().get_parent().get_node("big_bang").team_color_2 :
		team_number = 2
	if team_color == get_parent().get_parent().get_parent().get_node("big_bang").team_color_3 :
		team_number = 3
	if team_color == get_parent().get_parent().get_parent().get_node("big_bang").team_color_4 :
		team_number = 4

func set_random_course():
	var rd = RandomNumberGenerator.new()
	rd.randomize()
	var rrd = rd.randf_range(0, 360)
	rotation_degrees = rrd
	course = Vector2(speed, 0).rotated(rotation)
	last_course_change = 0
	
func change_course_or_not(delta):
	last_course_change += delta
	if last_course_change >= freakyness:
		set_random_course()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

# Below is same as above but synched with the physics engine and recommended in the doc for everything movement-related 
func _physics_process(delta):
	if time_since_spawn >= spawn_waiting:
		#Bellow is the process of moving
		change_course_or_not(delta)
		move_and_slide(course) # the funcs move_and_collide/move_and_slide are already defined in KinematicBody2D
	
		# Below is the process of bearing a child :
		if gestation == true:
			gestation_time += delta
			if gestation_time >= gestation_req_time:
				baby_birth()
				gestation = false
				gestation_time = 0 # Otherwise they'd make instant babies when touching each other after a first pregnancy
				get_node("tokens/pregnancy").visible = false
	
	# Below is the process of growing up :
	if age == "child":
		get_node("appearance").scale += Vector2(0.2, 0.2)*delta # progressive increase in appearance size
		freakyness = freakyness*1.5 # progressive increase in freakyness means less change of course
		speed = speed*1.5 # progressive increase in speed
		if get_node("appearance").scale >= Vector2(1, 1):
			age = "adult"
			get_node("appearance").scale = Vector2(1, 1) # as a security if ever it went a little up the line
			freakyness = adult_normal_freakyness # as a security
			speed = adult_normal_speed # as a security
				
	time_since_spawn += delta

func an_area_enters_my_body(area):
	if area.get_parent() != self: # This line prevents area of the same human (body and front areas) to interact with each other.
		if area.get_name() == "front_area" && age == "adult" && area.get_parent().age == "adult": # Fronts interact with bodies ; only adults can be killed or get pregnant but also kill or impregnate
			if area.get_parent().team_color == team_color: # If a front of the same color gets to a body (of the same color), they create a baby
				#print("we re ready for a baby")#needed only for test purposes
				if gestation == true:
					#print("but i have one already in the belly !")#needed only for test purposes
					pass
				if gestation == false:
					#print("they're ready for a baby !")#needed only for test purposes
					gestation = true
					get_node("tokens/pregnancy").visible = true
			if area.get_parent().team_color != team_color: # If an ennemy front gets to an ennmy body, the body bearer "dies"
				self.queue_free()
	#print("coucou moi meme")

func baby_birth():
	#print("here's the baby")#needed only for test purposes
	var individual_human = generic_human.instance() # create in the memory an individual human based on the generic human
	individual_human.position = position # Pour tester et voir où arrive le bébé
	individual_human.team_color = team_color # The baby takes the color of the bearer for now (no genetic mix)
	if team_color == "blue":
		var individual_texture = load("res://assets/square_32px_#7091F5_blue.png")
		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
	if team_color == "yellow":
		var individual_texture = load("res://assets/square_32px_#FABC5F_yellow.png")
		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
#	if team_color == "green": # The right assets are needed for this
#		var individual_texture = load("res://assets/###")
#		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
#	if team_color == "red":
#		var individual_texture = load("res://assets/###")
#		individual_human.get_node("appearance").get_node("body_appearance").set_texture(individual_texture)
	individual_human.age = "child"
	individual_human.freakyness = freakyness/2
	individual_human.speed = speed/2
	individual_human.get_node("appearance").scale = Vector2(0.1, 0.1)
	
	get_parent().add_child(individual_human) # we add it to our main scene at the right position
