extends Node2D
# Stocks and maybe modifies variables that influences the humans individual behaviors (such as war and peace for instance).

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Below are variables that are going to be used elsewhere - namely by humans - but need to be defined outside of humans (mainly because there's no human before bigbang but some UI elements are based on those variables anyway)
var adult_normal_freakyness = 2 # in seconds, time between two of the body's change of course
var spawn_waiting = 2 # in seconds, time after spawn before "activation" (movement only for now, but should be improve as well as far as interactions are concerned)

# Called when the node enters the scene tree for the first time.
func _ready():
	#print("bonjour")#needed only for test purposes
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
